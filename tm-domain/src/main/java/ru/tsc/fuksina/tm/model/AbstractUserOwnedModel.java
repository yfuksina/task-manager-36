package ru.tsc.fuksina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    private String userId;

}

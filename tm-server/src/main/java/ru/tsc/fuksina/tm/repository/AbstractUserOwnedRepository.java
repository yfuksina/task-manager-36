package ru.tsc.fuksina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.IUserOwnedRepository;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
      return models.stream()
              .filter(m -> userId.equals(m.getUserId()))
              .sorted(comparator)
              .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()) && userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @NotNull final M model) {
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final M model = findOneByIndex(userId, index);
        return remove(model);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}

package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.request.ProjectShowByIdRequest;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken(), id);
        @Nullable final Project project = getProjectEndpoint().showProjectById(request).getProject();
        showProject(project);
    }

}

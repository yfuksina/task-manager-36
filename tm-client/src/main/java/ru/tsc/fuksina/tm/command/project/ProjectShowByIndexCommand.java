package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.request.ProjectShowByIndexRequest;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show project by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken(), index);
        @Nullable final Project project = getProjectEndpoint().showProjectByIndex(request).getProject();
        showProject(project);
    }

}

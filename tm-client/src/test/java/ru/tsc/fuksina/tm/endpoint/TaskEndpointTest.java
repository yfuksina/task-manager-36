package ru.tsc.fuksina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.fuksina.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.fuksina.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.marker.SoapCategory;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.Task;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private String token;

    @Nullable
    private Task initTask;

    @Before
    public void init() {
        token = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        initTask =
                taskEndpoint.createTask(new TaskCreateRequest(token, "init task", "init")).getTask();
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest("", "name", "desc"))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(token, "", "desc"))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(token, "name", ""))
        );
        @NotNull final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(token, "name", "desc"));
        Assert.assertNotNull(response);
        Assert.assertEquals("name", response.getTask().getName());
    }

    @Test
    public void listTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest("", null)));
        @NotNull final TaskListResponse response = taskEndpoint.listTask(new TaskListRequest(token, null));
        Assert.assertNotNull(response);
        Assert.assertEquals(1, response.getTasks().size());
        @NotNull final TaskListResponse responseSort =
                taskEndpoint.listTask(new TaskListRequest(token, Sort.BY_CREATED));
        Assert.assertNotNull(responseSort);
        Assert.assertEquals(1, responseSort.getTasks().size());
    }

    @Test
    public void clearTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(new TaskClearRequest("")));
        @NotNull final TaskClearResponse response =
                taskEndpoint.clearTask(new TaskClearRequest(token));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest("", initTask.getId(), Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(token, "", Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(token, initTask.getId(), null)
                )
        );
        @NotNull final TaskChangeStatusByIdResponse response =
                taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(token, initTask.getId(), Status.IN_PROGRESS)
                );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest("", 0, Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, -1, Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 0, null)
                )
        );
        @NotNull final TaskChangeStatusByIndexResponse response =
                taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 0, Status.IN_PROGRESS)
                );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void removeTaskById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest("", initTask.getId()))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, "")));
        @NotNull final TaskRemoveByIdResponse response =
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, initTask.getId()));
        Assert.assertNotNull(response);
        Assert.assertNull(taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, initTask.getId())).getTask()
        );
    }

    @Test
    public void removeTaskByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest("", 0))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, null)));
        @NotNull final TaskRemoveByIndexResponse response =
                taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, 0));
        Assert.assertNotNull(response);
        Assert.assertNull(taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, initTask.getId())).getTask()
        );
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest("", initTask.getId()))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest(token, ""))
        );
        @NotNull final TaskShowByIdResponse response =
                taskEndpoint.showTaskById(new TaskShowByIdRequest(token, initTask.getId()));
        Assert.assertNotNull(response);
        Assert.assertEquals(initTask.getName(), response.getTask().getName());
    }

    @Test
    public void showTaskByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest("", 0))
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(token, null))
        );
        @NotNull final TaskShowByIndexResponse response =
                taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(token, 0));
        Assert.assertNotNull(response);
        Assert.assertEquals(initTask.getName(), response.getTask().getName());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest("", initTask.getId(), "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(token, "", "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(token, initTask.getId(), "", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(token, initTask.getId(), "new name", "")
                )
        );
        @NotNull final TaskUpdateByIdResponse response =
                taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(token, initTask.getId(), "new name", "new desc")
                );
        Assert.assertNotNull(response);
        Assert.assertEquals("new name", response.getTask().getName());
        Assert.assertEquals("new desc", response.getTask().getDescription());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest("", 0, "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(token, null, "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(token, 0, "", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(token, 0, "new name", "")
                )
        );
        @NotNull final TaskUpdateByIndexResponse response =
                taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(token, 0, "new name", "new desc")
                );
        Assert.assertNotNull(response);
        Assert.assertEquals("new name", response.getTask().getName());
        Assert.assertEquals("new desc", response.getTask().getDescription());
    }

    @Test
    public void showTaskByProjectId() {
        @NotNull final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();
        @NotNull final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();
        @NotNull final Project project = projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(token, 0)).getProject();
        projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, project.getId(), initTask.getId()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskByProjectId(
                        new TaskShowByProjectIdRequest("", project.getId())
                )
        );
        @NotNull final TaskShowByProjectIdResponse emptyListResponse = taskEndpoint.showTaskByProjectId(
                new TaskShowByProjectIdRequest(token, ""));
        Assert.assertNull(emptyListResponse.getTasks());
        @NotNull final TaskShowByProjectIdResponse response = taskEndpoint.showTaskByProjectId(
                new TaskShowByProjectIdRequest(token, project.getId()));
        Assert.assertNotNull(response.getTasks());
        Assert.assertEquals(1, response.getTasks().size());
    }

}

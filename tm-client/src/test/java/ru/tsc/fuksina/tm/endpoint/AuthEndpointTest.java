package ru.tsc.fuksina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.fuksina.tm.dto.request.UserLoginRequest;
import ru.tsc.fuksina.tm.dto.request.UserShowProfileRequest;
import ru.tsc.fuksina.tm.dto.response.UserLoginResponse;
import ru.tsc.fuksina.tm.dto.response.UserShowProfileResponse;
import ru.tsc.fuksina.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("", "test")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("test", "")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("user", "test")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("test", "user")));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserShowProfileRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserShowProfileRequest("token")));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        @NotNull final String token = response.getToken();
        @NotNull final UserShowProfileResponse profileResponse = authEndpoint.profile(new UserShowProfileRequest(token));
        Assert.assertTrue(profileResponse.getSuccess());
        Assert.assertNotNull(profileResponse.getUser());
        Assert.assertEquals("test", profileResponse.getUser().getLogin());
    }

}
